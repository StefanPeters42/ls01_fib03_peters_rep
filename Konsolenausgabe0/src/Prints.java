
public class Prints {

	public void printStars() {
		System.out.print("  **  \n*    *\n*    *\n  ** ");
	}
	
	
	public void printFakultaet() {
		String[] multis = new String[6];
		String current = "";
		
		for(int i = 1; i <= 5; i++) {
			if(i > 1) {
				current = current + " * " + i;
			} else {
				current = current + i;
			}
			multis[i] = current;
		}
		
		System.out.printf("%-5s = %-19s = %4d\n", "0!", "", 1);
		System.out.printf("%-5s = %-19s = %4d\n", "1!", multis[1], 1);
		System.out.printf("%-5s = %-19s = %4d\n", "2!", multis[2], 2);
		System.out.printf("%-5s = %-19s = %4d\n", "3!", multis[3], 6);
		System.out.printf("%-5s = %-19s = %4d\n", "4!", multis[4], 24);
		System.out.printf("%-5s = %-19s = %4d\n", "5!", multis[5], 120);
	}
}
