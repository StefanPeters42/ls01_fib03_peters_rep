import java.util.ArrayList;
import java.util.Scanner;
import java.util.Iterator;

public class Mittelwert {
	
	private static double mittelwert(double x, double y) {
		return (x + y) / 2.0;
	}
	
	private static double mittelwertList(ArrayList<Double> list) {
		if(list.isEmpty()) {
			return 0.0;
		} else {
			double listTotal = 0.0;
			int listCount = list.size();
			for (Iterator<Double> iter = list.iterator(); iter.hasNext(); ) {
			    listTotal += iter.next();
			}
			return listTotal / listCount;
		}
	}

	private static boolean isNumber(String s) {
		try {  
		    Double.parseDouble(s);  
		    return true;
		} catch(NumberFormatException e){  
		    return false;  
		}
	}
	
   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      double mList;
      Scanner myScanner = new Scanner(System.in);
      String input;
      
      ArrayList<Double> doubleList = new ArrayList<Double>();
      System.out.println("Enter a number (or c to commit): ");
      input = myScanner.next();
      while(isNumber(input)) {
    	  doubleList.add(Double.parseDouble(input));
    	  System.out.println("Enter a number (or c to commit): ");
    	  input = myScanner.next();
      }
      mList = mittelwertList(doubleList);
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      m = mittelwert(x, y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert ist %.2f\n", mList);
   }
}

