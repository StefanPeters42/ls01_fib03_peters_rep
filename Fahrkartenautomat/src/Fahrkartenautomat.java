﻿import java.util.Scanner;

class Fahrkartenautomat
{
	private static double fahrkartenbestellungErfassen(Scanner eingabe) {		
		double anzahl = 0.0;
		int auswahl = 0;
		double gesamtbetrag = 0.0;
		double[] preise = {2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
		String[] menu = {"Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC"};
		int[] eingabeAnzahl = new int[preise.length];
		
		for(int i = 0; i < preise.length; i++) {
			eingabeAnzahl[i] = 0;
		}
		
		System.out.print("Fahrkartenbestellvorgang:\n" +
					     "=========================\n");
		while(true) {
			anzahl = 0.0;
			auswahl = 0;
			System.out.print("\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
			for(int i = 0; i < menu.length; i++) {
				System.out.print(" " + menu[i] + " (" + (i + 1) + ")\n");
			}
			System.out.print(" Bezahlen (0)\n\n" +
							 "Ihre Wahl: ");			
		    auswahl = eingabe.nextInt();
		    
		    while(auswahl < 0 || auswahl > preise.length) {
		    	System.out.print("Fehler: ungültige Eingabe.\n\nIhre Wahl: ");
		    	auswahl = eingabe.nextInt();
		    }
		    
		    if(auswahl != 0) {
		    	System.out.print("Anzahl der Tickets: ");
			    anzahl = (int)eingabe.nextDouble();
			    while(anzahl < 1 || anzahl > 10) {
			    	System.out.println("\nFehler: Ungültige Anzahl der Tickets.\nDie Anzahl der Tickets muss zwischen 1 und 10 liegen.");
			    	System.out.print("Anzahl der Tickets: ");
				    anzahl = (int)eingabe.nextDouble();
			    }
			    eingabeAnzahl[auswahl - 1] += (int)anzahl;
		    } else {
		    	break;
		    }
		}
		for(int i = 0; i < preise.length; i++) {
			gesamtbetrag += preise[i] * eingabeAnzahl[i];
		}
		return gesamtbetrag;
	}
	
	/**
	 * Checks whether a given double array contains a given double value
	 * @param array the double array containing all values
	 * @param value the value to check for
	 * @return false if the array does not contain the value or is empty, true otherwise
	 */
	private static boolean arrayContains(double[] array, double value) {
		if(array.length <= 0) {
			return false;
		} else {
			for(int i = 0; i < array.length; i++) {
				if(array[i] == value) {
					return true;
				}
			}
			return false;
		}
	}
	
	private static double fahrkartenBezahlen(double zuZahlen, Scanner eingabe) {
		double eingezahlt = 0.0;
		double[] gueltigeWerte = {0.05, 0.1, 0.2, 0.5, 1.0, 2.0};
		double muenze = 0.0;
	    while(eingezahlt < zuZahlen)
	    {
	    	System.out.printf("Noch zu zahlen: %.2f Euro\n", zuZahlen - eingezahlt);
	    	System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	muenze = eingabe.nextDouble();
	    	if(!arrayContains(gueltigeWerte, muenze)) {
	    		System.out.print("Fehler: Münzwert wird nicht akzeptiert.\n\n");
	    	} else {
	    		eingezahlt += muenze;
	    	}
	    }
	    return eingezahlt - zuZahlen;
	}
	
	/**
	 * sets the thread asleep
	 * @param millisekunde the amount of time the thread is asleep
	 */
	private static void warte(int millisekunde) {
		try {
        	Thread.sleep(millisekunde);
        } catch (InterruptedException e) {
        	// TODO Auto-generated catch block
        	e.printStackTrace();
        }
	}
	
	/**
	 * @param no parameters needed
	 * @return no return value
	 */
	private static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	    for (int i = 0; i < 8; i++)
	    {
	    	System.out.print("=");
	        warte(250);
	    }
	    System.out.println("\n\n");
	}
	
	/**
	 * prints a string representing up to three coins
	 * @param betrag an array containing the values of the coins
	 * @param einheit an array containing the units of the coins
	 * @param anzahl the amount of coins, must be 1, 2 or 3
	 */
	private static void muenzeAusgeben(int[] betrag, String[] einheit, int anzahl) {
		if(anzahl <= 3 && anzahl >= 1) {
			if(betrag.length >= anzahl && einheit.length >= anzahl) {
				String[] result = new String[6];
				for(int i = 0; i < 6; i++) {
					result[i] = "";
				}
				for(int i = 0; i < anzahl; i++) {
					result[0] += "   * * *    ";
					result[1] += " *       *  ";
					result[2] += betrag[i] >= 10 ? "*    " + betrag[i] + "   * " : "*    " + betrag[i] + "    * ";
					result[3] += "*   " + einheit[i] + "  * ";
					result[4] += " *       *  ";
					result[5] += "   * * *    ";
				}
				for(int i = 0; i < 6; i++) {
					System.out.println(result[i]);
				}
			}
		}
	}
	
	/**
	 * 
	 * @param rückgabebetrag the amount of money that needs to be returned to the customer
	 * @return no return value
	 */
	private static void rueckgeldAusgeben(double rueckgabebetrag) {
		if(rueckgabebetrag > 0.0)
	    {
			System.out.println("Der Rückgabebetrag in Höhe von " + rueckgabebetrag + " EURO");
	    	System.out.println("wird in folgenden Münzen ausgezahlt:");
	    	
	    	int anzahl = 0;
	    	int[] betrag = new int[3];
	    	String[] einheit = new String[3];

	        while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
	        {
	        	betrag[anzahl] = 2;
	        	einheit[anzahl] = "EURO";
	        	anzahl++;
	        	if(anzahl >= 3) {
	        		muenzeAusgeben(betrag, einheit, anzahl);
	        		anzahl = 0;
	        	}
	        	rueckgabebetrag -= 2.0;
	        	rueckgabebetrag = Math.round(rueckgabebetrag * 100) * 0.01;
	        }
	        while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
	        {
	        	betrag[anzahl] = 1;
	        	einheit[anzahl] = "EURO";
	        	anzahl++;
	        	if(anzahl >= 3) {
	        		muenzeAusgeben(betrag, einheit, anzahl);
	        		anzahl = 0;
	        	}
	        	rueckgabebetrag -= 1.0;
	        	rueckgabebetrag = Math.round(rueckgabebetrag * 100) * 0.01;
	        }
	        while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
	        {
	        	betrag[anzahl] = 50;
	        	einheit[anzahl] = "CENT";
	        	anzahl++;
	        	if(anzahl >= 3) {
	        		muenzeAusgeben(betrag, einheit, anzahl);
	        		anzahl = 0;
	        	}
	        	rueckgabebetrag -= 0.5;
	        	rueckgabebetrag = Math.round(rueckgabebetrag * 100) * 0.01;
	        }
	        while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
	        {
	        	betrag[anzahl] = 20;
	        	einheit[anzahl] = "CENT";
	        	anzahl++;
	        	if(anzahl >= 3) {
	        		muenzeAusgeben(betrag, einheit, anzahl);
	        		anzahl = 0;
	        	}
	        	rueckgabebetrag -= 0.2;
	        	rueckgabebetrag = Math.round(rueckgabebetrag * 100) * 0.01;
	        }
	        while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
	        {
	        	betrag[anzahl] = 10;
	        	einheit[anzahl] = "CENT";
	        	anzahl++;
	        	if(anzahl >= 3) {
	        		muenzeAusgeben(betrag, einheit, anzahl);
	        		anzahl = 0;
	        	}
	        	rueckgabebetrag -= 0.1;
	        	rueckgabebetrag = Math.round(rueckgabebetrag * 100) * 0.01;
	        }
	        while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
	        {
	        	betrag[anzahl] = 5;
	        	einheit[anzahl] = "CENT";
	        	anzahl++;
	        	if(anzahl >= 3) {
	        		muenzeAusgeben(betrag, einheit, anzahl);
	        		anzahl = 0;
	        	}
	        	rueckgabebetrag -= 0.05;
	        	rueckgabebetrag = Math.round(rueckgabebetrag * 100) * 0.01;
	        }
	        if(anzahl > 0) {
        		muenzeAusgeben(betrag, einheit, anzahl);
        	}
	    }
	}
	
    public static void main(String[] args)
    {
       Scanner eingabe = new Scanner(System.in);
       double zuZahlenderBetrag; 
       double rueckgabebetrag;
       
       while(true) {
	       zuZahlenderBetrag = fahrkartenbestellungErfassen(eingabe);
	
	       // Geldeinwurf
	       // -----------
	       rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, eingabe);
	
	       // Fahrscheinausgabe
	       // -----------------
	       fahrkartenAusgeben();
	
	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       
	       rueckgeldAusgeben(rueckgabebetrag);
	
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n\n");
       }
    }
}