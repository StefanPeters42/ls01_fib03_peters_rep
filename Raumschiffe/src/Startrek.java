import java.util.ArrayList;

/**
 * Die Klasse "Startrek" stellt den "Weltraum" dar, in dem Instanzen der Klasse "Raumschiff" aufeinander treffen k�nnen.<br>
 * Sie enth�lt die main Methode.
 * @author Stefan Peters
 */
public class Startrek {

	/**
	 * Initialisiert die Raumschiffe entsprechend des Arbeitsauftrages.
	 * @param klingonen Das Raumschiff der Klingonen.
	 * @param romulaner Das Raumschiff der Romulaner.
	 * @param vulkanier Das Raumschiff der Vulkanier.
	 */
	public static void initRaumschiffe(Raumschiff klingonen, Raumschiff romulaner, Raumschiff vulkanier) {
		ArrayList<String> bc = new ArrayList<String>();
		
		klingonen.setPhotonTorpedos(1);
		klingonen.setEnergy(100);
		klingonen.setShields(100);
		klingonen.setShell(100);
		klingonen.setLifeSupport(100);
		klingonen.setName("IKS Hegh'ta");
		klingonen.setRepairAndroids(2);
		klingonen.setBroadcastCommunicator(bc);
		
		klingonen.addLoad(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLoad(new Ladung("Bat'leth Klingonen Schwert", 200));
		
		romulaner.setPhotonTorpedos(2);
		romulaner.setEnergy(100);
		romulaner.setShields(100);
		romulaner.setShell(100);
		romulaner.setLifeSupport(100);
		romulaner.setName("IRW Khazara");
		romulaner.setRepairAndroids(2);
		romulaner.setBroadcastCommunicator(bc);
		
		romulaner.addLoad(new Ladung("Borg-Schrott", 5));
		romulaner.addLoad(new Ladung("Rote Materie", 2));
		romulaner.addLoad(new Ladung("Plasma-Waffe", 50));
		
		vulkanier.setPhotonTorpedos(0);
		vulkanier.setEnergy(80);
		vulkanier.setShields(80);
		vulkanier.setShell(50);
		vulkanier.setLifeSupport(100);
		vulkanier.setName("Ni'Var");
		vulkanier.setRepairAndroids(5);
		vulkanier.setBroadcastCommunicator(bc);
		
		vulkanier.addLoad(new Ladung("Forschungssonde", 35));
		vulkanier.addLoad(new Ladung("Photonentorpedos", 3));
	}
	
	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff();
		Raumschiff romulaner = new Raumschiff();
		Raumschiff vulkanier = new Raumschiff();
		
		initRaumschiffe(klingonen, romulaner, vulkanier);
		
		//Der Ablauf des Aufeinandertreffens der 3 Raumschiffe entsprechend der Aufgabenstellung.
		klingonen.shootPhotonTorpedos(romulaner);
		romulaner.shootPhaserCannon(klingonen);
		vulkanier.sendMessageToAll("Gewalt ist nicht logisch");
		klingonen.showState();
		klingonen.logLoadToConsole();
		vulkanier.startRepairAction(true, true, true, 5);
		vulkanier.applyPhotonTorpedos(3);
		vulkanier.cleanUpLoad();
		klingonen.shootPhotonTorpedos(romulaner);
		klingonen.shootPhotonTorpedos(romulaner);
		klingonen.showState();
		klingonen.logLoadToConsole();
		romulaner.showState();
		romulaner.logLoadToConsole();
		vulkanier.showState();
		vulkanier.logLoadToConsole();
	}

}