import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * Die Klasse "Raumschiff" stellt ein Raumschiff aus Startrek dar, mit den Attributen Name, Energie, Schilde, H�lle, Lebenserhaltungssysteme,
 * Photonentorpedos, Reparaturandroiden, Broadcastkommunikator und Ladungsliste, sowie verschiedenen Funktionalit�ten.
 * @author Stefan Peters
 */
public class Raumschiff {

	private String name = "";
	private int energy = 0;
	private int shields = 0;
	private int lifeSupport = 0;
	private int shell = 0;
	private int photonTorpedos = 0;
	private int repairAndroids = 0;
	private ArrayList<String> broadcastCommunicator = null;
	private ArrayList<Ladung> loads = null;
	
	//constructors
	
	/**
	 * Kreiert ein Raumschiff mit den Defaultwerten:<br>
	 * <strong>Name</strong> "New Spaceship"<br>
	 * <strong>Energie</strong> 100<br>
	 * <strong>Schilde</strong> 100<br>
	 * <strong>H�lle</strong> 100<br>
	 * <strong>Lebenserhaltungssysteme</strong> 100<br>
	 * <strong>Photonentorpedos</strong> 0<br>
	 * <strong>Reperaturandroiden</strong> 0<br>
	 * <strong>Broadcastkommunikator</strong> null
	 */
	public Raumschiff() {
		this.name = "New Spaceship";
		this.energy = 100;
		this.shields = 100;
		this.lifeSupport = 100;
		this.shell = 100;
		this.photonTorpedos = 0;
		this.repairAndroids = 0;
		this.broadcastCommunicator = null;
		this.loads = new ArrayList<Ladung>();
	}
	
	/**
	 * Kreiert ein Raumschiff mit den �bergebenen Werten.
	 * @param name Der Name des Raumschiffs.
	 * @param energy Die Energie des Raumschiffs in Prozent.
	 * @param shields Die Schilde des Raumschiffs in Prozent.
	 * @param lifeSupport Die Lebenserhaltungssysteme des Raumschiffs in Prozent.
	 * @param shell Die H�lle des Raumschiffs in Prozent.
	 * @param photonTorpedos Die Anzahl der geladenen Photonentorpedos des Raumschiffs.
	 * @param repairAndroids Die Anzahl der Reparaturandroiden des Raumschiffs.
	 * @param broadcastCommunicator Der gemeinsame Broadcastkommunikator aller Raumschiffe.
	 */
	public Raumschiff(String name, int energy, int shields, int lifeSupport, int shell, int photonTorpedos,
			int repairAndroids, ArrayList<String> broadcastCommunicator) {
		this.name = name;
		this.energy = energy;
		this.shields = shields;
		this.lifeSupport = lifeSupport;
		this.shell = shell;
		this.photonTorpedos = photonTorpedos;
		this.repairAndroids = repairAndroids;
		this.broadcastCommunicator = broadcastCommunicator;
		this.loads = new ArrayList<Ladung>();
	}

	//getters and setters
	
	/**
	 * Gibt den Namen des Raumschiffs zur�ck.
	 * @return Der Name des Raumschiffs.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt den Namen des Raumschiffs.
	 * @param name Der zu setzende Name des Raumschiffs.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gibt die Energie des Raumschiffs zur�ck.
	 * @return Die Energie des Raumschiffs.
	 */
	public int getEnergy() {
		return energy;
	}

	/**
	 * Setzt die Energie des Raumschiffs.
	 * @param energy Die zu setzende Energie des Raumschiffs.
	 */
	public void setEnergy(int energy) {
		this.energy = energy;
	}

	/**
	 * Gibt die Schilde des Raumschiffs zur�ck.
	 * @return Die Schilde des Raumschiffs.
	 */
	public int getShields() {
		return shields;
	}

	/**
	 * Setzt die Schilde des Raumschiffs.
	 * @param shields Die zu setzenden Schilde des Raumschiffs.
	 */
	public void setShields(int shields) {
		this.shields = shields;
	}

	/**
	 * Gibt die Lebenserhaltungssysteme des Raumschiffs zur�ck.
	 * @return Die Lebenserhaltungssysteme des Raumschiffs.
	 */
	public int getLifeSupport() {
		return lifeSupport;
	}

	/**
	 * Setzt die Lebenserhaltungssysteme des Raumschiffs.
	 * @param lifeSupport Die zu setzenden Lebenserhaltungssysteme des Raumschiffs.
	 */
	public void setLifeSupport(int lifeSupport) {
		this.lifeSupport = lifeSupport;
	}

	/**
	 * Gibt die H�lle des Raumschiffs zur�ck.
	 * @return Die H�lle des Raumschiffs.
	 */
	public int getShell() {
		return shell;
	}

	/**
	 * Setzt die H�lle des Raumschiffs.
	 * @param shell Die zu setzende H�lle des Raumschiffs.
	 */
	public void setShell(int shell) {
		this.shell = shell;
	}

	/**
	 * Gibt die Anzahl der geladenen Photonentorpedos des Raumschiffs zur�ck.
	 * @return Die Anzahl der geladenen Photonentorpedos des Raumschiffs.
	 */
	public int getPhotonTorpedos() {
		return photonTorpedos;
	}

	/**
	 * Setzt die Anzahl der Photonentorpedos des Raumschiffs.
	 * @param photonTorpedos Die zu setzende Anzahl der Photonentorpedos des Raumschiffs.
	 */
	public void setPhotonTorpedos(int photonTorpedos) {
		this.photonTorpedos = photonTorpedos;
	}

	/**
	 * Gibt die Anzahl der Reparaturandroiden des Raumschiffs zur�ck.
	 * @return Die Anzahl der Reparaturandroiden des Raumschiffs.
	 */
	public int getRepairAndroids() {
		return repairAndroids;
	}

	/**
	 * Setzt die Anzahl der Reparaturandroiden des Raumschiffs.
	 * @param repairAndroids Die zu setzende Anzahl der Reparaturandroiden des Raumschiffs.
	 */
	public void setRepairAndroids(int repairAndroids) {
		this.repairAndroids = repairAndroids;
	}

	/**
	 * Gibt den Broadcastkommunikator des Raumschiffs zur�ck.
	 * @return Der Broadcastkommunikator des Raumschiffs.
	 */
	public ArrayList<String> getBroadcastCommunicator() {
		return broadcastCommunicator;
	}

	/**
	 * Setzt den Broadcastkommunikator des Raumschiffs.
	 * @param broadcastCommunicator Der zu setzende Broadcastkommunikator des Raumschiffs.
	 */
	public void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
		this.broadcastCommunicator = broadcastCommunicator;
	}
	
	//methods
	
	/**
	 * Schie�t einen geladenen Photonentorpedo des Raumschiffs auf das Zielraumschiff ab.<br>
	 * Wenn keine Photonentorpedos geladen sind oder das Zielraumschiff null ist, wird "-=*Click*=-" ausgegeben.<br>
	 * Ansonsten wird ein geladener Photonentorpedo verbraucht und die Methode "registerHit()" des Zielraumschiffs aufgerufen.
	 * @param target Das Zielraumschiff des abgeschossenen Photonentorpedos.
	 */
	public void shootPhotonTorpedos(Raumschiff target) {
		if(this.getPhotonTorpedos() <= 0 || target == null) {
			this.sendMessageToAll("-=*Click*=-");
		} else {
			this.setPhotonTorpedos(this.getPhotonTorpedos() - 1);
			this.sendMessageToAll("Photonentorpedo abgeschossen");
			target.registerHit(0);
		}
	}
	
	/**
	 * Schie�t mit der Phaserkanone des Raumschiffs auf das Zielraumschiff udn verbraucht Energie.<br>
	 * Wenn die Energie des Raumschiffs unter 50% oder das Zielraumschiff null ist, wird "-=*Click*=-" ausgegeben.<br>
	 * Ansonsten wird die Energie des Raumschiffs um 50% reduziert und die Methode "registerHit()" des Zielraumschiffs aufgerufen.
	 * @param target Das Zielraumschiff der abgeschossenen Phaserkanone.
	 */
	public void shootPhaserCannon(Raumschiff target) {
		if(this.getEnergy() < 50 || target == null) {
			this.sendMessageToAll("-=*Click*=-");
		} else {
			this.setEnergy(this.getEnergy() - 50);
			this.sendMessageToAll("Phaserkanone abgeschossen");
			target.registerHit(1);
		}
	}
	
	/**
	 * Registriert den Treffer einer auf dieses Raumschiff abgeschossenen Waffe und berechnet die Auswirkung.
	 * @param weaponType Die Art der abgeschossenen Waffe. Photonentorpedo (<strong>0</strong>) oder Phaserkanone (<strong>1</strong>).
	 */
	public void registerHit(int weaponType) {
		if(this.getShields() >= 0) {
			this.setShields(this.getShields() - 50);
		}
		if(this.getShields() < 0) {
			this.setShell(this.getShell() - 50);
			this.setEnergy(this.getEnergy() - 50);
		}
		if(this.getShell() <= 0) {
			this.setLifeSupport(0);
			this.sendMessageToAll("Lebenserhaltungssysteme sind vollst�ndig zerst�rt");
		}
	}
	
	/**
	 * F�gt eine Nachricht zum Broadcastkommunikator hinzu. Die Nachricht hat folgende Form: "Raumschiffname: Nachricht".
	 * @param message Die Nachricht, die zum Broadcastkommunikator hinzugef�gt wird.
	 */
	public void sendMessageToAll(String message) {
		this.getBroadcastCommunicator().add(this.getName() + ": " + message);
	}
	
	/**
	 * F�gt eine Ladung zum Laderaum des Raumschiffs hinzu.
	 * @param load Die Ladung, die hinzugef�gt wird.
	 */
	public void addLoad(Ladung load) {
		this.loads.add(load);
	}
	
	/**
	 * Gibt den Inhalt des Broadcastkommunikators (alle von allen Raumschiffen versendete Nachrichten) auf der Konsole aus.
	 */
	public void returnLog() {
		ArrayList<String> bc = this.getBroadcastCommunicator();
		System.out.println("\nLogbuch");
		String s = "";
		for(int i = 0; i < 20; i++) {
			s += "-";
		}
		System.out.println(s);
		for(Iterator<String> iterator = bc.iterator(); iterator.hasNext();) {
			String current = iterator.next();
			System.out.println(current);
		}
		System.out.println(s);
	}
	
	/**
	 * L�dt eine Anzahl Photonentorpedos aus dem Laderaum des Raumschiffs in die Torpedorohre.<br>
	 * Wenn der Laderaum des Raumschiffs keine Ladung vom Typ "Photonentorpedos" enth�lt, wird eine Nachricht auf der Konsole ausgegeben.<br>
	 * Wenn der Laderaum des Raumschiffs Ladungen vom Typ "Photonentorpedos" enth�lt, werden alle Photonentorpedos vom Laderum in
	 * die Torpedorohre geladen, bis zu einer durch "amount" definierten maximalen Anzahl.<br>
	 * Wenn "amount" kleiner oder gleich 0 ist, passiert nichts.
	 * @param amount Die maximale Anzahl an Photonentorpedos, die vom Laderaum in die Torpedorohre geladen werden sollen. Muss gr��er als 0 sein.
	 */
	public void applyPhotonTorpedos(int amount) {
		if(amount > 0) {
			int pTfound = 0;
			for(Iterator<Ladung> iterator = this.loads.iterator(); iterator.hasNext();) {
				Ladung current = iterator.next();
				if(current.getType().equals("Photonentorpedos")) {
					if(current.getAmount() <= amount - pTfound) {
						pTfound += current.getAmount();
						current.setAmount(0);
					} else {
						current.setAmount(current.getAmount() - (amount - pTfound));
						pTfound = amount;
					}
					if(pTfound >= amount) {
						break;
					}
				}
			}
			if(pTfound <= 0) {
				System.out.println("\nKeine Photonentorpedos gefunden!");
				this.sendMessageToAll("-=*Click*=-");
			} else {
				this.setPhotonTorpedos(this.getPhotonTorpedos() + pTfound);
				System.out.print("\n" + pTfound + " Photonentorpedo");
				if(pTfound > 1) {
					System.out.print("s");
				}
				System.out.println(" eingesetzt");
				this.cleanUpLoad();
			}
		}
	}
	
	/**
	 * Repariert die Schilde, Energie und/oder H�lle des Raumschiffs unter Einsatz der Reparaturandroiden.<br>
	 * Die H�he, um die die Systeme verst�rkt werden, ist von der Anzahl gew�hlter Systeme und aktivierter Reparaturandroiden abh�ngig.
	 * @param shields Wenn shields auf true gesetzt ist, werden die Schilde des Raumschiffs verst�rkt.
	 * @param energy Wenn energy auf true gesetzt ist, wird die Energie des Raumschiffs verst�rkt.
	 * @param shell Wenn shell auf true gesetzt ist, wird die H�lle des Raumschiffs verst�rkt.
	 * @param androidCount Die maximale Anzahl eingesetzter Reparaturandroiden. Muss gr��er als 0 sein.
	 */
	public void startRepairAction(boolean shields, boolean energy, boolean shell, int androidCount) {
		Random rng = new Random();
		int rand = rng.nextInt(101);
		int droids = androidCount > this.getRepairAndroids() ? this.getRepairAndroids() : androidCount;
		int systems = 0;
		systems += shields ? 1 : 0;
		systems += energy ? 1 : 0;
		systems += shell ? 1 : 0;
		if(systems > 0 && droids > 0) {
			int repairPower = (rand * droids) / systems;
			if(shields) {
				this.setShields(this.getShields() + repairPower);
			}
			if(energy) {
				this.setEnergy(this.getEnergy() + repairPower);
			}
			if(shell) {
				this.setShell(this.getShell() + repairPower);
			}
		}
	}
	
	/**
	 * Gibt den aktuellen Zustand (Name, Energie, Schilde, Lebenserhaltungssysteme, H�lle, geladene Photonentorpedos, Reparaturandroiden)
	 * auf der Konsole aus.
	 */
	public void showState() {
		System.out.println("\nRaumschiffstatus");
		String s = "";
		for(int i = 0; i < 20; i++) {
			s += "-";
		}
		System.out.println(s);
		System.out.println("Name: " + this.getName());
		System.out.println("Energie: " + this.getEnergy());
		System.out.println("Schilde: " + this.getShields());
		System.out.println("Lebenserhaltungssysteme: " + this.getLifeSupport());
		System.out.println("H�lle: " + this.getShell());
		System.out.println("Photonentorpedos: " + this.getPhotonTorpedos());
		System.out.println("Reparaturandroiden: " + this.getRepairAndroids());
		System.out.println(s);
	}
	
	/**
	 * Gibt alle Ladungen des Raumschiffs als Ladungsverzeichnis auf der Konsole aus.
	 */
	public void logLoadToConsole() {
		System.out.println("\nLadungsverzeichnis");
		String s = "";
		for(int i = 0; i < 20; i++) {
			s += "-";
		}
		System.out.println(s);
		if(loads.isEmpty()) {
			System.out.println("Das Schiff hat keine Ladung geladen.");
		} else {
			for(Iterator<Ladung> iterator = this.loads.iterator(); iterator.hasNext();) {
				Ladung current = iterator.next();
				System.out.println(current.getType() + ": " + current.getAmount());
			}
		}
		System.out.println(s);
	}
	
	/**
	 * R�umt den Laderaum des Raumschiffs auf, indem Ladungen, deren St�ckzahl <= 0 ist, entfernt werden.
	 */
	public void cleanUpLoad() {
		for(Iterator<Ladung> iterator = this.loads.iterator(); iterator.hasNext();) {
			Ladung current = iterator.next();
			if(current.getAmount() <= 0) {
				iterator.remove();				
			}
		}
	}
}
