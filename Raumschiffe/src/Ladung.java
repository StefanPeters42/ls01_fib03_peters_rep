
/**
 * Die Klasse "Ladung" stellt eine Ladung dar, die von Instanzen der Klasse "Raumschiff" in deren Laderaum geladen werden k�nnen.<br>
 * Sie hat die Attribute Bezeichnung und Anzahl.
 * @author Stefan Peters
 */
public class Ladung {
	
	private String type = "";
	private int amount = 0;
	
	/**
	 * Kreiert eine Ladung mit den Defaultwerten:<br>
	 * <strong>Bezeichnung</strong> "New Load"<br>
	 * <strong>Anzahl</strong> 1
	 */
	public Ladung() {
		this.type = "New Load";
		this.amount = 1;
	}

	/**
	 * Kreiert eine Ladung mit den �bergebenen Werten.
	 * @param type Die Bezeichnung der Ladung.
	 * @param amount Die Anzahl der Ladung.
	 */
	public Ladung(String type, int amount) {
		this.type = type;
		this.amount = amount;
	}

	/**
	 * Gibt die Bezeichnung der Ladung zur�ck.
	 * @return Die Bezeichnung der Ladung.
	 */
	public String getType() {
		return type;
	}

	/**
	 * Setzt die Bezeichnung der Ladung.
	 * @param type Die zu setzende Bezeichnung der Ladung.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gibt die Anzahl der Ladung zur�ck.
	 * @return Die Anzahl der Ladung.
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * Setzt die Anzahl der Ladung.
	 * @param amount Die zu setzende Anzahl der Ladung.
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}
}
